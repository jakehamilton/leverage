# Photo Op
Version 0.1.1

# What's New In 0.1.1?
* Routes now have optional dependency injection
* Passport is now available and configured
* MongoDB is now available and configured
* User model created and available

# What's New In 0.1.0?
* Express Server now implemented
* Express Session Middleware now implemented
* Dynamic Server Routes now implemented
* Static Assets are now available in the `public` directory
* Dynamic Routes can be defined in the `routes` directory
* Views can now be defined in the `views` directory
* Gulpfile added

# Installation
Make sure you have [MongoDB](https://www.mongodb.com) installed

* Clone this repo
```
git clone https://jakehamilton@bitbucket.org/jakehamilton/leverage.git
cd leverage
```
* Install dependencies
```
npm install
```
* Build project
```
gulp build
```

# Development
* Watch source files
```
gulp
```
* Run project

with [nodemon](https://www.npmjs.com/package/nodemon)

```
nodemon index.js // Watches for file changes and reloads
```

or without

```
node index.js
```

# Contributing
* Semicolons are required
* ES6 where applicable is preferred
* Lower case underscore naming convention for variables, functions, etc.
* Keep things nice and tidy
* Comments for documentation are required
* Other comments should provide meaningful insight into a piece of code
