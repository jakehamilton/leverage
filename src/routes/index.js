'use strict';

/**
* @class route
* @description Handles an individual route for the app
*/
class route {
  /**
  * @constructor
  * @description Setup modules and environment for the route
  */
  constructor() {
    /**
    * @member {String} route_name The uri of the route
    * @example this.route_name = '/';
    */
    this.route_name = '/';
    /**
    * @member {String} route_method The method of the route
    * @example this.route_method = 'get';
    */
    this.route_method = 'get';
    /**
    * @member {Array} route_dependencies The dependencies require for the route
    * @example this.route_dependencies = ['passport']
    */
    this.route_dependencies = [];
  }

  /**
  * @method config
  * @description Configure additional dependencies
  * @param {Object} dependencies An object containing additional dependencies for the route
  */
  config(dependencies) {

  }

  /**
  * @method handler
  * @description Handler for each request to the route
  * @param {Object} req The request object
  * @param {Object} res The Response object
  * @callback next Calls the next handler in line
  * @example
  * handler(req, res, next) {
  *   res.send("Hello World");
  *   next();
  * }
  */
  handler(req, res, next) {
    res.render('index');
    next();
  }
}

module.exports = new route();
