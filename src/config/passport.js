'use strict';

/**
* @class config
* @description Config file for passport
*/
class config {
  /**
  * @constructor
  * @description Setup modules and environment
  */
  constructor() {
    this.user = require('../models/user.js');
    this.local_strategy = require('passport-local').Strategy;
  }

  /**
  * @method init
  * @description Initialize the config
  * @param {Object} passport The passport object to configure
  */
  init(passport) {
    passport.serializeUser((user, done) => done(null, user.id));
    passport.deserializeUser((id, done) => {
      this.user.model.findById(id, (err, user) => done(err, user));
    });

    // New signup strategy
    passport.use('local-signup', new this.local_strategy({
      usernameField: 'username',
      passwordField: 'password',
      passReqToCallback: true
    }, function(req, username, password, done) {
      this.user.model.findOne({ 'local.username' : username }, (err, user) => {
        if (err) return done(err);
        if (user) {
          console.log('User already exists');
          return done(null);
        } else {
          let User = new this.user.model();
          User.local.username = username;
          User.local.password = User.generate_hash(password);

          User.save((err) => {
            if (err) throw err;
            return done(null, User);
          });
        }
      });
    }.bind(this)));

    passport.use('local-login', new this.local_strategy({
      usernameField: 'username',
      passwordField: 'password',
      passReqToCallback: true
    }, function(req, username, password, done) {
      this.user.model.findOne({ 'local.username' : username }, function(err, user) {
        if (err) return done(err);
        if (!user) {
          return done(null, false);
        }
        if (!user.valid_password(password)) {
          return done(null, false);
        }
        return done(null, user);
      });
    }.bind(this)));
  }
}

module.exports = new config();
