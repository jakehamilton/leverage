'use strict';

/**
* @class config
* @description Config file for MongoDB
*/
class config {
  /**
  * @constructor
  * @description Setup environment
  */
  constructor() {
    this.url = 'mongodb://localhost/op';
  }

  /**
  * @method init
  * @description Initialize the config
  * @param {Object} mongoose The mongoose object to configure
  */
  init(mongoose) {
    mongoose.connect(this.url);
  }
}

module.exports = new config();
