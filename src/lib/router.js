'use strict';

/**
* @class router
* @description Handles routing routing the express app
*/
class router {
  /**
  * @constructor
  * @description Setup modules and environment for router
  */
  constructor() {
    this.jetpack = require('fs-jetpack');
    this.routes = [];
  }

  /**
  * @method get_routes
  * @description Get routes from `/routes`
  * @return {Promise.<Array, Error>} routes Promise resolves to array of routes if successful
  */
  get_routes() {
    return new Promise((resolve, reject) => {
      if (this.routes.length == 0) {
        this.jetpack.inspectTreeAsync('routes')
          .then((data) => data.children)
          .then((files) => {
            files.forEach((file) => {
              if (/.+\.js$/g.test(file.name) && file.type === 'file') {
                this.routes.push(require('../routes/' + file.name));
              }
            });
            resolve(this.routes);
          })
          .catch(reject);
      } else {
        resolve(this.routes);
      }
    });
  }
}

module.exports = new router();
