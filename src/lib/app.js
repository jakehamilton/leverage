'use strict';

/**
* @class app
* @description Handles iteraction with modules
*/
class app {
  /**
  * @constructor
  * @description Setup modules and environment for app
  * @param {Object} [options] Optional Settings
  * @param {Bool} [options.should_init_server = true] Whether or not to initialize the server on creation
  */
  constructor(options = {
    should_init_server: true
  }) {
    this.server = new require('./server.js');

    if (options.should_init_server) {
      this.start_server();
    }
  }

  /**
  * @method start_server
  * @description Start the server
  * @param {Object} [options] Optional Settings
  * @param {Number} [options.port] The port to listen on (Defaults to '80' in production and '3000' in development)
  * @return {void}
  */
  start_server(options = {
    port: process.env.NODE_ENV === 'production' ? 80 : 3000
  }) {
    this.server.init(options);
  }
}

module.exports = new app();
