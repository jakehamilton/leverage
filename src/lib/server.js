'use strict';

/**
* @class server
* @description Handles the http server instance
*/
class server {
  /**
  * @constructor
  * @description Setup modules and environment for server
  */
  constructor() {
    this.path = require('path');
    this.router = require('./router.js');
    this.express = require('express');
    this.session = require('express-session');
    this.passport = require('passport');
    this.mongoose = require('mongoose');
    this.body_parser = require('body-parser');
    this.cookie_parser = require('cookie-parser');

    /**
    * @member {Object} app Express instance
    */
    this.app = this.express();

    /**
    * @member {Object} models MongoDB Models
    * @member {Object} models.user User model
    */
    this.models = {
      user: require('../models/user.js')
    }

    // Passport Config
    require('../config/passport.js').init(this.passport);

    // DB Config
    require('../config/db.js').init(this.mongoose);

    // App config
    this.app.set('view engine', 'jade');
    this.app.use(this.body_parser.json());
    this.app.use(this.body_parser.urlencoded({
      extended: true
    }));
    this.app.use(this.cookie_parser());
    this.app.use(this.express.static(this.path.resolve(__dirname, '..', 'public/')));
    this.app.use(this.session({
      resave: false,
      saveUninitialized: true,
      secret: 'superdupersecretstringwahoooooo'
    }));
    this.app.use(this.passport.initialize());
    this.app.use(this.passport.session());
  }

  /**
  * @method init
  * @description Initialize the server
  * @param {Object} [options] Optional Settings
  * @param {Number} [options.port] Port to listen to
  */
  init(options = { port }) {
    this.router.get_routes()
      .then((routes) => {
        routes.forEach((route) => {
          let payload = {};
          route.route_dependencies = route.route_dependencies || [];
          route.route_dependencies.forEach((dependency) => {
            payload[dependency] = this[dependency] || null;
          });
          route.config(payload);
          this.app[route.route_method.toLowerCase()](route.route_name, route.handler.bind(route));
        });
        return true;
      })
      .then(() => {
        this.app.listen(options.port);
      })
      .catch((error) => console.warn);
  }
}

module.exports = new server();
