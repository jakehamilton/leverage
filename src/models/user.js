'use strict';

class model {
  constructor() {
    // Fix binding error
    const bcrypt = require('bcrypt');

    this.mongoose = require('mongoose');

    this.schema = this.mongoose.Schema({
      local: {
        email: String,
        username: String,
        password: String,
        reset_id: String
      },
      facebook: {
        id: String,
        token: String,
        email: String,
        name: String
      },
      google: {
        id: String,
        token: String,
        email: String,
        name: String
      }
    });


    this.schema.methods.generate_hash = function(password) {
      return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
    }

    this.schema.methods.valid_password = function(password) {
      return bcrypt.compareSync(password, this.local.password);
    }

    this.model = this.mongoose.model('User', this.schema);
  }
}

module.exports = new model();
