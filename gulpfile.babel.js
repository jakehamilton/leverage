'use strict';

const gulp = require('gulp');
const watch = require('gulp-watch');
const batch = require('gulp-batch');
const babel = require('gulp-babel');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');

gulp.task('watch', ['build'], () => {
  watch('src/lib/*.js', batch((events, done) => {
    gulp.start('lib', done);
  }));
  watch('src/routes/*.js', batch((events, done) => {
    gulp.start('routes', done);
  }));
  watch('src/client/*.js', batch((events, done) => {
    gulp.start('client', done);
  }));
  watch('src/models/*.js', batch((events, done) => {
    gulp.start('models', done);
  }));
  watch('src/config/*.js', batch((events, done) => {
    gulp.start('config', done);
  }));
});

gulp.task('build', ['lib', 'routes', 'client', 'models', 'config']);

gulp.task('lib', () => {
  return gulp.src('src/lib/*.js')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('lib/'));
});

gulp.task('routes', () => {
  return gulp.src('src/routes/*.js')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('routes/'));
});

gulp.task('client', () => {
  return gulp.src('src/client/*.js')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/assets/js'));
});

gulp.task('models', () => {
  return gulp.src('src/models/*.js')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('models/'));
});

gulp.task('config', () => {
  return gulp.src('src/config/*.js')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('config/'));
});

gulp.task('default', ['watch']);
